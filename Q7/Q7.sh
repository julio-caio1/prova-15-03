#!/bin/bash

h=$(date +%u)

q=$(( (3 - h + 7) % 7))

# Adicionar a diferença

p=$(date -d "+$q" +%d/%B/%Y)

echo "Próxima: $p"
