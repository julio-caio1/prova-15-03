#!/bin/bash

read -p "Digite 4 nomes [deve conter espaço em branco entre cada um]: " a b c d

y=$(date +'%d/%B/%Y')

mkdir "$a" "$b" "$c" "$d"

echo "Diretório: $a
==========================
Data: $y" > "$a/README.md"

echo "Diretório: $b
==========================
Data: $y" > "$b/README.md"

echo "Diretório: $c
==========================
Data: $y" > "$c/README.md"

echo "Diretório: $d
==========================
Data: $y" > "$d/README.md"
