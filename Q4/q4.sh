#!/bin/bash

d=$(df -h)
p=$(lscpu)
u=$(lsusb)
s=$(lspci)

echo "Informações do HW
================================="
echo -e "\033[92mDisco: $d"
sleep 2
clear

echo -e "\033[1mProcessador: $p\033[m"
sleep 2
clear

echo -e "\033[94mDispositivos usb: $u\033[m"
sleep 2
clear

echo -e "\033[92mPCI $s\033[m"
sleep 2

