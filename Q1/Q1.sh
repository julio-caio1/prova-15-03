#!/bin/bash
# Autor: Julio Caio

echo -e "\033[1mComo um fantasma que se refugia\033[m"
sleep 0.76
echo -e "\033[4;90mNa solidão da natureza morta,\033[m"
sleep 0.75
echo -e "\033[0;31mPor trás dos ermos túmulos, um dia,\033[m"
sleep 0.74
echo -e "\033[94mEu fui refugiar-me à tua porta!\033[m"
sleep 0.72
echo -e "\033[92mFazia frio e o frio que fazia\033[m"
